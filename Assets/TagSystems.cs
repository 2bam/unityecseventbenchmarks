﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Jobs;
using Unity.Mathematics;
using Unity.Entities;
using Unity.Collections;
using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;

// RUN ORDER
//
// Sys_Tag_...
// Init (once) > Raise > RaiseBarrier > Process > ProcessBarrier

namespace Tag {

	//Different namespace, different type!
	struct Dat_UniqueForThisSystem : IComponentData {
		byte _bogus;
	}

	public class Sys_Tag_Init : Sys_Base {
		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object) });
			if(Config.instance.sysTag)
				CreateEntities();
		}
	}

	[UpdateAfter(typeof(Sys_Tag_Init))]
	public class Sys_Tag_Raise : Sys_Base {
		[Inject] Sys_Tag_RaiseBarrier _barrier;

		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object) });
			CreateRNGs();
		}

		//Not supported: [BurstCompile]
		struct Job : IJobProcessComponentDataWithEntity<Dat_UniqueForThisSystem, Dat_Object> {
			[NativeDisableParallelForRestriction] public NativeArray<Unity.Mathematics.Random> rngs;
			[NativeSetThreadIndex] int thread;
			public float eventChance;
			public EntityCommandBuffer.Concurrent buffer;

			public void Execute(Entity entity, int index, [ReadOnly] ref Dat_UniqueForThisSystem dummy, ref Dat_Object data) {
				var rng = rngs[thread];
				if(rng.NextFloat() < eventChance) {
					//Event occurred
					buffer.AddComponent(index, entity, new Dat_EventData { type = EventType.TheEvent, magic = 123 });
				}
				rngs[thread] = rng;
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			return new Job {
				rngs = _rngs
				, eventChance = _eventChance
				, buffer = _barrier.CreateCommandBuffer().ToConcurrent()
			}.Schedule(this, inputDeps);
		}
	}

	[UpdateAfter(typeof(Sys_Tag_Raise))]
	public class Sys_Tag_RaiseBarrier : BarrierSystem { }

	[UpdateAfter(typeof(Sys_Tag_RaiseBarrier))]
	//public class Sys_Tag_Process : JobComponentSystem {
	//	[Inject] EntityManager _emgr;
	//	[Inject] Sys_Tag_ProcessBarrier _barrier;

	//	//Not supported: [BurstCompile]
	//	struct Job : IJobProcessComponentDataWithEntity<Dat_UniqueForThisSystem, Dat_Object, Dat_EventData> {
	//		public EntityCommandBuffer.Concurrent buffer;

	//		public void Execute(Entity entity, int index, [ReadOnly] ref Dat_UniqueForThisSystem dummy, ref Dat_Object obj, ref Dat_EventData evData) {
	//			//Respond to event
	//			if(evData.type == EventType.TheEvent) {
	//				evData.type = EventType.None;
	//				obj.number++;
	//				obj.magic = evData.magic;
	//			}
	//			buffer.RemoveComponent<Dat_EventData>(index, entity);
	//		}
	//	}

	//	protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
	//		return new Job {
	//			buffer = _barrier.CreateCommandBuffer().ToConcurrent()
	//		}.Schedule(this, inputDeps);
	//	}
	//}
	public class Sys_Tag_Process : Sys_Base {
		[Inject] Sys_Tag_ProcessBarrier _barrier;

		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object), typeof(Dat_EventData) });
		}

		//Not supported: [BurstCompile]
		struct Job : IJobParallelFor {
			public EntityCommandBuffer.Concurrent buffer;
			public ComponentDataArray<Dat_Object> objectArray;
			public ComponentDataArray<Dat_EventData> evDataArray;
			public EntityArray entities;

			public void Execute(int index) {

				Dat_Object obj = objectArray[index];
				Dat_EventData evData = evDataArray[index];

				//Respond to event
				if(evData.type == EventType.TheEvent) {
					evData.type = EventType.None;
					obj.number++;
					obj.magic = evData.magic;
				}

				objectArray[index] = obj;
				//evDataArray[index] = evData;

				buffer.RemoveComponent<Dat_EventData>(index, entities[index]);
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			return new Job {
				buffer = _barrier.CreateCommandBuffer().ToConcurrent()
				, entities = _group.GetEntityArray()
				, objectArray = _group.GetComponentDataArray<Dat_Object>()
				, evDataArray = _group.GetComponentDataArray<Dat_EventData>()
			}.Schedule(_group.CalculateLength(), _batchCount, inputDeps);
		}
	}

	[UpdateAfter(typeof(Sys_Tag_Process))]
	public class Sys_Tag_ProcessBarrier : BarrierSystem { }

}