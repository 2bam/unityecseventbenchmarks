﻿namespace EvQueueHM {
	using System.Collections.Generic;
	using Unity.Jobs;
	using Unity.Entities;
	using Unity.Collections;
	using Unity.Burst;
	using UnityEngine.Profiling;
	using Unity.Collections.LowLevel.Unsafe;

	//Different namespace, different type!
	struct Dat_UniqueForThisSystem : IComponentData {
		byte _bogus;
	}

	public class Sys_EvQueueHM_Init : Sys_Base {
		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object) });
			if(Config.instance.sysEvQueueHM)
				CreateEntities();
		}
	}

	[UpdateAfter(typeof(Sys_EvQueueHM_Init))]
	public class Sys_EvQueueHM_Raise : Sys_Base {
		[Inject] Sys_EvQueueHM_Process sysProcess;

		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object) });
			//Debug.Log("Raise Create");
			CreateRNGs();
		}

		[BurstCompile]
		struct Job : IJobProcessComponentDataWithEntity<Dat_UniqueForThisSystem, Dat_Object> {
			[NativeDisableParallelForRestriction] public NativeArray<Unity.Mathematics.Random> rngs;
			[NativeSetThreadIndex] int thread;
			public float eventChance;

			public NativeMultiHashMap<Entity, Dat_EventData>.Concurrent _mapMT;
			public NativeQueue<Entity>.Concurrent _entsMT;

			public void Execute(Entity entity, int index, [ReadOnly] ref Dat_UniqueForThisSystem dummy, ref Dat_Object obj) {
				var rng = rngs[thread];
				if(rng.NextFloat() < eventChance) {
					//Event occurred

					_mapMT.Add(entity, new Dat_EventData { type = EventType.TheEvent, magic = 123 });
					_entsMT.Enqueue(entity);

					//.RaiseEvent(entity, new Dat_EventData { type = EventType.TheEvent, magic = 123 });
				}
				rngs[thread] = rng;
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			return new Job {
				rngs = _rngs
				, eventChance = _eventChance
				, _mapMT = sysProcess._map.ToConcurrent()
				, _entsMT = sysProcess._ents.ToConcurrent()
			}.Schedule(this, inputDeps);
		}
	}

	[AlwaysUpdateSystem]	//<-- Important
	[UpdateAfter(typeof(Sys_EvQueueHM_Raise))]
	public class Sys_EvQueueHM_Process : Sys_Base {

		//TODO: Profile this: Could probably do without hashmultimap,
		//		but executing all events over the same entity in sequence
		//		probably have some cache improvement with ComponentDataFromEntity
		//		Events-per-entity volume could be important parameter here.

		//TODO: do custom NativeContainer for these that has a RaiseEvent func...
		public NativeMultiHashMap<Entity, Dat_EventData> _map;		
		public NativeQueue<Entity> _ents;		//Can't get keys from multi hash map

		public NativeArray<byte> _sparseSet;

		protected override void OnCreateManager() {
			_map = new NativeMultiHashMap<Entity, Dat_EventData>(Config.instance.HashMapSize, Allocator.Persistent);
			_ents = new NativeQueue<Entity>(Allocator.Persistent);
			//_mapMT = _map.ToConcurrent();
			//_entsMT = _ents.ToConcurrent();

			int MaxEntities = Config.instance.numEntites;
			_sparseSet = new NativeArray<byte>(MaxEntities, Allocator.Persistent);

			//Dummy call to link dependencies with other systems (because we don't actually use any components):
			GetComponentGroup(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object) });
		}

		protected override void OnDestroyManager() {
			base.OnDestroyManager();
			_map.Dispose();
			_ents.Dispose();
			_sparseSet.Dispose();
		}

		//Warning, reuses _set
		protected bool QueueToArrayUnique<T>(NativeQueue<T> q, Allocator allocType, out NativeArray<T> array, out NativeSlice<T> slice) where T : struct {
			T item;
			if(!q.TryDequeue(out item)) {
				array = default(NativeArray<T>);
				slice = default(NativeSlice<T>);
				return false;
			}

			Profiler.BeginSample("QueueToArrayUnique");
			
			array = new NativeArray<T>(q.Count+1, allocType);   //Worst case they're all unique (NOTICE +1 because we already dequeued one)
			int index = 0;
			var set = new HashSet<T>();
			do {
				if(!set.Add(item))
					continue;       //Repeated
				array[index++] = item;
			}
			while(q.TryDequeue(out item));
			slice = array.Slice(0, index);
			Profiler.EndSample();
			return true;
		}

		//public void RaiseEvent(Entity ent, Dat_EventData evData) {
		//	_mapMT.Add(ent, evData);
		//	_entsMT.Enqueue(ent);
		//}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			//Act as a barrier, no other way around this to access _map/_ents
			//inputDeps.Complete();

			//_mapMT = _map.ToConcurrent();
			//_entsMT = _ents.ToConcurrent();

			//Update2();
			//return new JobHandle();

			return Update3(inputDeps);
		}

		//protected override void OnUpdateImpl() {
		//	//Update3().Complete();
		//	//_mapMT = _map.ToConcurrent();
		//	//_entsMT = _ents.ToConcurrent();
		//	Update2();
		//}

#if true
		//FIXME: Of course this will fuck up, I don't need to iterate over all entities with Dat_Object... just with ParallelFor over _ents (avoid repetition with _shitSet)
		//-----------not gonna work, jost IJob normally
		[BurstCompile]
		struct JobExecuteEvent : IJobParallelFor {
			[ReadOnly] public NativeMultiHashMap<Entity, Dat_EventData> map;

			[ReadOnly]
			public NativeSlice<Entity> entityArray;	//NOTE: Can't use directly a queue because 1) not sure MT, 2) could have multiple reads from same entity in hashmap, so must be unique array to ensure objectArray concurrency ok

			[NativeDisableContainerSafetyRestriction]		//This is fiiine, each "index" is of a completly different entity.
			public ComponentDataFromEntity<Dat_Object> objectArray;

			public void Execute(int index) {
				Dat_EventData evt;
				NativeMultiHashMapIterator<Entity> it;
				var key = entityArray[index];

				if(!map.TryGetFirstValue(key, out evt, out it))
					return; //This should only happen for repeated entities

				do {
					//Execute event
					if(evt.type == EventType.TheEvent) {
						var obj = objectArray[key];
						obj.number++;
						obj.magic = evt.magic;
						//evt.type = EventType.None;		//No use, will be nuked shortly
						//_map.SetValue(evt, it);
						objectArray[key] = obj;
					}
				} while(map.TryGetNextValue(out evt, ref it));
				
				//Is this thread safe? 
				//map.Remove(key);		//I think this streamlines better than removing per iterator, even if it has to GetHashCode from Entity (int, so fast).
			}
		}

		[BurstCompile]
		struct JobClear : IJob {
			public NativeMultiHashMap<Entity, Dat_EventData> map;
			[DeallocateOnJobCompletion] public NativeArray<Entity> entityFullArray;
			public void Execute() {
				//This takes a lot of time!!! although realloc of the hash map takes the same time...
				//100k takes .60ms
				//10k takes .09ms
				//1k takes 0.01ms
				//TODO: Maybe try to first queue events, then in a smaller loop with a smaller hashmap: q->hm->exec->q(cont)->hm->exec
				map.Clear();
			}
		}

		JobHandle Update3(JobHandle inputDeps) {
			//TODO: set _***MT to new obj(empty ctor = nullish?) as debug measure while running this?
			if(_ents.Count == 0)		//TODO: Replace with "HasPending" when available...
				return new JobHandle();

			var objectArray = GetComponentDataFromEntity<Dat_Object>();
			NativeArray<Entity> entityFullArray;
			NativeSlice<Entity> entityArray;

			//We need to do this on the main thread because there is no concurrent dequeue in NativeQueue
			//So might as well use a HashSet...
			//TODO: Some main-thread/ECS filtering could be done right here (e.g. for hybrid interaction)
			//Should always return true...
			QueueToArrayUnique(_ents, Allocator.TempJob, out entityFullArray, out entityArray);
			
			var deps = new JobExecuteEvent {
				objectArray = objectArray
				, map = _map
				, entityArray=entityArray
			}.Schedule(entityArray.Length, Config.instance.batchCount, inputDeps);

			deps = new JobClear {
				map = _map
				, entityFullArray = entityFullArray
			}.Schedule(deps);

			return deps;

			//deps.Complete();
			//Profiler.BeginSample("Realloc hashmap");
			//entityFullArray.Dispose();
			//_map.Dispose();
			//_map = new NativeMultiHashMap<Entity, Dat_EventData>(Config.instance.hashMapSize, Allocator.Persistent);        //FIXME: Magic arbitrary number...
			//Profiler.EndSample();
			//return default(JobHandle);
		}
#endif

		protected void Update2() {
			Entity key;
			if(!_ents.TryDequeue(out key))
				return;

			var objectArray = GetComponentDataFromEntity<Dat_Object>();
			var set = new HashSet<Entity>();
			do {
				//Tried queue->array->sorted, all native, but a hashset is faster. (See Update1())
				if(!set.Add(key))
					continue;

				Dat_EventData evt;
				NativeMultiHashMapIterator<Entity> it;

				if(!_map.TryGetFirstValue(key, out evt, out it))
					continue;//This shouldn't happen...
				do {
					//Execute event
					if(evt.type == EventType.TheEvent) {
						var obj = objectArray[key];
						obj.number++;
						obj.magic = evt.magic;
						//evt.type = EventType.None;		//No use, will be nuked shortly
						//_map.SetValue(evt, it);
						objectArray[key] = obj;
					}
				} while(_map.TryGetNextValue(out evt, ref it));

				_map.Clear();
				_ents.Clear();
			} while(_ents.TryDequeue(out key));
		}

#if false

		Comp _comp = new Comp();

		class Comp : IComparer<Entity> {
			public int Compare(Entity x, Entity y) {
				return x.Index - y.Index;
			}
		}

		protected void Update1() {
			//TODO: set _***MT to new obj(empty ctor = nullish?) as debug measure while running this?
			var objectArray = GetComponentDataFromEntity<Dat_Object>();

			NativeArray<Entity> array;
			if(ToNativeArray(_ents, Allocator.Temp, out array)) {
				Dat_EventData evt;
				NativeMultiHashMapIterator<Entity> it;

				//Jobified SortingUtilities is internal...
				array.Sort(_comp);

				Entity last = Entity.Null;
				for(int i = 0; i < array.Length; i++) {
					var key = array[i];

					if(last == key)     //Skip repeated entities
						continue;

					last = key;
					if(!_map.TryGetFirstValue(key, out evt, out it))
						continue;//This shouldn't happen...
					do {
						//Execute event
						if(evt.type == EventType.TheEvent) {
							var obj = objectArray[key];
							obj.number++;
							obj.magic = evt.magic;
							//evt.type = EventType.None;		//No use, will be nuked shortly
							//_map.SetValue(evt, it);
							objectArray[key] = obj;
						}
					} while(_map.TryGetNextValue(out evt, ref it));
				}

				array.Dispose();

				_map.Clear();
				_ents.Clear();
			}
		}
#endif
	}
}