﻿namespace EvQueue {
	using Unity.Jobs;
	using Unity.Entities;
	using Unity.Collections;
	using Unity.Burst;
	using Unity.Collections.LowLevel.Unsafe;

	//Different namespace, different type!
	struct Dat_UniqueForThisSystem : IComponentData {
		byte _bogus;
	}

	public class Sys_EvQueue_Init : Sys_Base {
		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object) });
			if(Config.instance.sysEvQueue)
				CreateEntities();
		}
	}

	[UpdateAfter(typeof(Sys_EvQueue_Init))]
	public class Sys_EvQueue_Raise : Sys_Base {
		[Inject] Sys_EvQueue_Process sysProcess;

		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object) });
			//Debug.Log("Raise Create");
			CreateRNGs();
		}

		[BurstCompile]
		struct Job : IJobProcessComponentDataWithEntity<Dat_UniqueForThisSystem, Dat_Object> {
			[NativeDisableParallelForRestriction] public NativeArray<Unity.Mathematics.Random> rngs;
			[NativeSetThreadIndex] int thread;
			public float eventChance;

			public NativeQueue<Dat_EventWithSource>.Concurrent _entsMT;

			public void Execute(Entity entity, int index, [ReadOnly] ref Dat_UniqueForThisSystem dummy, ref Dat_Object obj) {
				var rng = rngs[thread];
				if(rng.NextFloat() < eventChance) {
					//Event occurred
					_entsMT.Enqueue(new Dat_EventWithSource { source = entity, type = EventType.TheEvent, magic = 123 });
				}
				rngs[thread] = rng;
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			inputDeps.Complete();
			return new Job {
				rngs = _rngs
				, eventChance = _eventChance
				, _entsMT = sysProcess._queue.ToConcurrent()
			//}.Schedule(this, inputDeps);
			}.Schedule(this);
		}
	}

	[AlwaysUpdateSystem]    //<-- Important
	[UpdateAfter(typeof(Sys_EvQueue_Raise))]
	public class Sys_EvQueue_Process : Sys_Base {
		public NativeQueue<Dat_EventWithSource> _queue;     //Can't get keys from multi hash map

		protected override void OnCreateManager() {
			_queue = new NativeQueue<Dat_EventWithSource>(Allocator.Persistent);

			//Dummy call to link dependencies with other systems (because we don't actually use any components):
			GetComponentGroup(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object) });
		}

		protected override void OnDestroyManager() {
			base.OnDestroyManager();
			_queue.Dispose();
		}

		//Can't use parallel because there is no parallel dequeueing provided in NativeQueue.
		[BurstCompile]
		struct Job : IJob {
			public NativeQueue<Dat_EventWithSource> queue;
			public ComponentDataFromEntity<Dat_Object> objectArray;
			[ReadOnly] public Dat_EventWithSource firstEvent;

			public void Execute() {
				var ev = firstEvent;
				do {
					if(ev.type == EventType.TheEvent) {
						var obj = objectArray[ev.source];
						obj.magic = ev.magic;
						obj.number++;
						objectArray[ev.source] = obj;
					}
				} while(queue.TryDequeue(out ev));
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			inputDeps.Complete();
			Dat_EventWithSource ev;
			//Having a concurrent "HasPending" property would be nice (Could try-catch Peek or use Count which is not as fast, too)
			if(_queue.TryDequeue(out ev)) {
				//Avoid superfluous calls if queue empty.
				var job = new Job {
					queue = _queue
					, objectArray = GetComponentDataFromEntity<Dat_Object>()
					, firstEvent = ev    //Pass over the dequeued event to unify logic
				};
				//return job.Schedule(inputDeps);
				return job.Schedule();
			}
			return inputDeps;
		}
	}
}