﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Jobs;
using Unity.Mathematics;
using Unity.Entities;
using Unity.Collections;
using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;

using BovineLabs.Common.Native;
//using BovineLabs.Common.Utility;

// RUN ORDER
//
// Sys_Batch...
// Init (once) > Raise > CreateEvents > Process

namespace Batch {

	//Different namespace, different type!
	struct Dat_UniqueForThisSystem : IComponentData {
		byte _bogus;
	}

	[System.Serializable]
	public struct Dat_BatchEvent : IComponentData {	//Same as Dat_EventWithSource but blabla, uniqueness...
		public Entity source;
		public EventType type;
		public byte magic;
	}	

	public class Sys_Batch_Init : Sys_Base {
		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object) });
			if(Config.instance.sysBatch)
				CreateEntities();
		}
	}

	[UpdateAfter(typeof(Sys_Batch_Init))]
	public class Sys_Batch_Raise : Sys_Base {

		[Inject] Sys_Batch_CreateEvents	 _batch;
		NativeQueue<Dat_BatchEvent> _queue;
		
		protected override void OnCreateManager() {
			var compTypes = new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object) };
			Init(compTypes);
			CreateRNGs();
			_batch.AddComponentsDependency(compTypes);
		}

		[BurstCompile]
		struct Job : IJobProcessComponentDataWithEntity<Dat_UniqueForThisSystem, Dat_Object> {
			[NativeDisableParallelForRestriction] public NativeArray<Unity.Mathematics.Random> rngs;
			[NativeSetThreadIndex] int thread;
			public float eventChance;
			public NativeQueue<Dat_BatchEvent>.Concurrent _queueEventsMT;
			
			public void Execute(Entity entity, int index, [ReadOnly] ref Dat_UniqueForThisSystem dummy, ref Dat_Object obj) {
				var rng = rngs[thread];
				if(rng.NextFloat() < eventChance) {
					//Event occurred
					_queueEventsMT.Enqueue(new Dat_BatchEvent { source = entity, type = EventType.TheEvent, magic = 123 });
				}
				rngs[thread] = rng;
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			return new Job {
				rngs = _rngs
				, eventChance = _eventChance
				, _queueEventsMT = _batch.GetEventBatch<Dat_BatchEvent>().ToConcurrent()
			}.Schedule(this, inputDeps);
		}
	}

	[UpdateAfter(typeof(Sys_Batch_Raise))]
	public class Sys_Batch_CreateEvents : BatchSystem { }

	[UpdateAfter(typeof(Sys_Batch_CreateEvents))]
	public class Sys_Batch_Process : Sys_Base {

		protected override void OnCreateManager() {
			base.OnCreateManager();
			Init(new ComponentType[] { /*typeof(Dat_UniqueForThisSystem),*/ typeof(Dat_BatchEvent) });
		}

		[BurstCompile]
		struct Job : IJob {
			public ComponentDataFromEntity<Dat_Object> objectByEntArray;
			[ReadOnly] public ComponentDataArray<Dat_BatchEvent> eventArray;

			public void Execute() {
				for(int i = 0; i < eventArray.Length; i++) {
					var evData = eventArray[i];
					if(evData.type == EventType.TheEvent) {
						var obj = objectByEntArray[evData.source];

						obj.number++;
						obj.magic = evData.magic;
						//evData.type = EventType.None; //No need, will be nuked on next BatchSystem update

						objectByEntArray[evData.source] = obj;
					}
				}
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			return new Job {
				objectByEntArray = GetComponentDataFromEntity<Dat_Object>()
				, eventArray = _group.GetComponentDataArray<Dat_BatchEvent>()
			}.Schedule(inputDeps);
		}
	}

}


