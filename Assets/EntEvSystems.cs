﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Jobs;
using Unity.Mathematics;
using Unity.Entities;
using Unity.Collections;
using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;

// RUN ORDER
//
// Sys_EntEv...
// Init (once) > Raise > CreateEvents > Process

namespace EntEv {

	//Different namespace, different type!
	struct Dat_UniqueForThisSystem : IComponentData {
		byte _bogus;
	}

	public class Sys_EntEv_Init : Sys_Base {
		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object) });
			if(Config.instance.sysEntEv)
				CreateEntities();
		}
	}

	[UpdateAfter(typeof(Sys_EntEv_Init))]
	public class Sys_EntEv_Raise : Sys_Base {

		[Inject] Sys_EntEv_CreateEvents sysCreateEvents;

		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object) });
			CreateRNGs();
		}

		[BurstCompile]
		struct Job : IJobProcessComponentDataWithEntity<Dat_UniqueForThisSystem, Dat_Object> {
			[NativeDisableParallelForRestriction] public NativeArray<Unity.Mathematics.Random> rngs;
			[NativeSetThreadIndex] int thread;
			public float eventChance;
			public NativeQueue<Dat_EventWithSource>.Concurrent _queueEventsMT;

			public void Execute(Entity entity, int index, [ReadOnly] ref Dat_UniqueForThisSystem dummy, ref Dat_Object obj) {
				var rng = rngs[thread];
				if(rng.NextFloat() < eventChance) {
					//Event occurred
					_queueEventsMT.Enqueue(new Dat_EventWithSource { source = entity, type = EventType.TheEvent, magic = 123 });
				}
				rngs[thread] = rng;
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			return new Job {
				rngs = _rngs
				, eventChance = _eventChance
				, _queueEventsMT = sysCreateEvents.queueEventsMT
			}.Schedule(this, inputDeps);
		}
	}

	[AlwaysUpdateSystem]	//<-- Important! as we will be creating entities from a queue.
	[UpdateAfter(typeof(Sys_EntEv_Raise))]
	public class Sys_EntEv_CreateEvents : Sys_Base {

		public NativeQueue<Dat_EventWithSource>.Concurrent queueEventsMT;
		NativeQueue<Dat_EventWithSource> _queueEvents;
		NativeArray<Entity> _entities;		//Keep entity-event list until next frame to batch-destroy them

		protected override void OnCreateManager() {
			base.OnCreateManager();

			// SUPER IMPORTANT: This might be only working because typeof(Dat_UniqueForThisSystem) is mutual between systems!!!!!
			GetComponentGroup(typeof(Dat_UniqueForThisSystem), typeof(Dat_Object));	//Dummy call to link

			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_EventWithSource) });
			_queueEvents = new NativeQueue<Dat_EventWithSource>(Allocator.Persistent);
			CreateRNGs();

			queueEventsMT = _queueEvents.ToConcurrent();
		}

		protected override void OnDestroyManager() {
			base.OnDestroyManager();
			_queueEvents.Dispose();
			DestroyPreviousEntities();
		}

		void DestroyPreviousEntities() {
			if(_entities.IsCreated) {
				_emgr.DestroyEntity(_entities);
				_entities.Dispose();		//TODO: Try NativeList clearing getting a slice?
			}
		}

		[BurstCompile]
		struct JobParallel : IJobParallelFor {
			public NativeQueue<Dat_EventWithSource> queueEvents;
			public ComponentDataFromEntity<Dat_EventWithSource> eventByEntArray;
			[ReadOnly] public NativeArray<Entity> entities;
			public void Execute(int index) {
				Dat_EventWithSource ev;
				while(queueEvents.TryDequeue(out ev))
					eventByEntArray[entities[index+1]] = ev;		//NOTICE: index+1 because we already set the first one on main thread OnUpdateImpl...
			}
		}
		
		[BurstCompile]
		struct JobSerial : IJob {
			public NativeQueue<Dat_EventWithSource> queueEvents;
			public ComponentDataFromEntity<Dat_EventWithSource> eventByEntArray;
			[ReadOnly] public NativeArray<Entity> entities;
			[ReadOnly] public Dat_EventWithSource firstEvent;
			public void Execute() {
				int index = 0;
				Dat_EventWithSource ev = firstEvent;
				do {
					eventByEntArray[entities[index++]] = ev;
				} while(queueEvents.TryDequeue(out ev));
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			inputDeps.Complete();		//Need all raising done before creating the events...

			//TODO: Add profiling to 

			//This needs to happen on the main thread...
			DestroyPreviousEntities();

			//TODO: Make IJob

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Works here and not in EvQueueSystems... race condition????^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//Because CreateEvents in between Process???

			Dat_EventWithSource ev;
			//WORKAROUND: Can't use Count before TryDequeue, form an internal bug maybe, clearing concurrent buffers etc.
			if(_queueEvents.TryDequeue(out ev)) {
				var remaining = _queueEvents.Count;
				_entities = new NativeArray<Entity>(remaining + 1, Allocator.TempJob);     //NOTICE: remaining+1 because we just dequeued one.
				_emgr.CreateEntity(_archetype, _entities);
				var eventByEntArray = GetComponentDataFromEntity<Dat_EventWithSource>();

				////Serial version
				//int index = 0;
				//do {
				//	eventByEntArray[_entities[index++]] = ev;
				//} while(_queueEvents.TryDequeue(out ev));

				//Job-serial version.
				//Process remaining
				var job = new JobSerial {
					firstEvent = ev
					, queueEvents = _queueEvents
					, eventByEntArray = eventByEntArray
					, entities = _entities
					};
				return job.Schedule(inputDeps);

				////Parallel version.
				//ERROR: Must turn queue to array first to allow concurrency...
				////Process first
				//eventByEntArray[_entities[0]] = ev;
				////Process remaining
				//var job = new Job { queueEvents = _queueEvents, eventByEntArray = eventByEntArray };
				//return job.Schedule(remaining, Config.instance.batchCount);
			}

			return new JobHandle();
		}
	}

	#if false
	[UpdateAfter(typeof(Sys_EntEv_CreateEvents))]
	public class Sys_EntEv_Process : JobComponentSystem {
		[BurstCompile]
		struct Job : IJobProcessComponentDataWithEntity<Dat_UniqueForThisSystem, Dat_EventWithSource> {

			//FIXME!!!: No concurrent access to this, this is problematic...
			//			unless we can batch (DynamicBuffer?) Dat_EventWithSources by Entity, we can never be sure there won't be race conditions

			public ComponentDataFromEntity<Dat_Object> objectByEntArray;
			public void Execute(Entity entity, int index, [ReadOnly] ref Dat_UniqueForThisSystem dummy, [ReadOnly] ref Dat_EventWithSource evData) {
				if(evData.type == EventType.TheEvent) {
					var obj = objectByEntArray[evData.source];

					obj.number++;
					obj.magic = evData.magic;
					//evData.type = EventType.None; //No need, will be nuked on next CreateEvents system update

					objectByEntArray[evData.source] = obj;
				}
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			return new Job {
				objectByEntArray = GetComponentDataFromEntity<Dat_Object>()
			}.Schedule(this, inputDeps);
		}
	}
	#endif

	[UpdateAfter(typeof(Sys_EntEv_CreateEvents))]
	public class Sys_EntEv_Process : Sys_Base {

		protected override void OnCreateManager() {
			base.OnCreateManager();
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_EventWithSource) });
		}

		[BurstCompile]
		struct Job : IJob {
			public ComponentDataFromEntity<Dat_Object> objectByEntArray;
			[ReadOnly] public ComponentDataArray<Dat_EventWithSource> eventArray;

			public void Execute() {
				for(int i = 0; i < eventArray.Length; i++) {
					var evData = eventArray[i];
					if(evData.type == EventType.TheEvent) {
						var obj = objectByEntArray[evData.source];

						obj.number++;
						obj.magic = evData.magic;
						//evData.type = EventType.None; //No need, will be nuked on next CreateEvents system update

						objectByEntArray[evData.source] = obj;
					}
				}
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			return new Job {
				objectByEntArray = GetComponentDataFromEntity<Dat_Object>()
				, eventArray = _group.GetComponentDataArray<Dat_EventWithSource>()
			}.Schedule(inputDeps);
		}
	}

}


