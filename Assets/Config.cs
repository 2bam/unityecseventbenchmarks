﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

using System;
using System.Reflection;

public class Config : MonoBehaviour {
	[Header("SET ALL BEFORE PLAY")]
	public int numEntites = 50000;
	[SerializeField] int hashMapSize = 50000;
	[Range(0f, 1f)] public float eventChance = 0.01f;
	public int batchCount = 64;
	public bool forceWaitJobs = false;
	public bool hashMapSizeAccordingToChanceAndEntities = false;

	[Header("System groups")]
	public bool sysPoll = true;
	public bool sysPollShared = true;
	public bool sysTag = true;
	public bool sysTagShared = true;
	public bool sysEvQueue = true;
	public bool sysEvQueueHM = true;
	public bool sysEntEv = true;
	public bool sysBatch = true;
	public bool sysDispatch = true;

	public int HashMapSize {
		get {
			return Config.instance.hashMapSizeAccordingToChanceAndEntities
							  ? (int)(Config.instance.numEntites * 1.2f * Config.instance.eventChance)
							  : Config.instance.hashMapSize
							  ;
		}
	}

	static Config _instance;
	static public Config instance {
		get {
			if(!_instance)
				_instance = FindObjectOfType<Config>();
			return _instance;
		}
	}

	private void Start() {
		//TODO: Instead of destroying, disable all and just create enabled.

		var fields = typeof(Config).GetFields(BindingFlags.Instance | BindingFlags.Public);
		//var steps = new[] { "Init", "Raise", "RaiseBarrier", "CreateEvents", "Process", "ProcessBarrier" };
		var toDisable = new HashSet<string>();
		foreach(var field in fields) {
			if(field.Name.StartsWith("sys")) {
				var enable = (bool)field.GetValue(this);
				if(!enable) {
					var group = field.Name.Substring(3);
					toDisable.Add(group);
				}
			}
		}
		foreach(var t in Assembly.GetExecutingAssembly().GetTypes()) {
			if(toDisable.Contains(t.Namespace)) {
				if(typeof(ComponentSystemBase).IsAssignableFrom(t)) {
					var sys = World.Active.GetExistingManager(t) as  ComponentSystemBase;
					if(sys != null) {
						sys.Enabled = false;       //Had to set this or I get an error
						World.Active.DestroyManager(sys);

						//FIXME: I don't know why even after this, slivers of dead systems are still present in the profiler...
					}
				}
			}
		}
	}

}
