﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Unity.Jobs;
using Unity.Mathematics;
using Unity.Entities;
using Unity.Collections;
using Unity.Burst;
using UnityEngine;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs.LowLevel.Unsafe;
using UnityEngine.Profiling;

abstract public class Sys_Base: JobComponentSystem {
	protected NativeArray<Unity.Mathematics.Random> _rngs;
	protected float _eventChance;
	protected int _batchCount;
	protected EntityArchetype _archetype;
	protected ComponentGroup _group;
	[Inject] protected EntityManager _emgr;
	string _systemGroup;

	public Sys_Base() {
		_systemGroup = GetType().Namespace;
	}

	protected void CreateRNGs() {
		_rngs = new NativeArray<Unity.Mathematics.Random>(JobsUtility.MaxJobThreadCount, Allocator.Persistent);
		for(int i = 0; i<JobsUtility.MaxJobThreadCount; i++)
			_rngs[i] = new Unity.Mathematics.Random((uint)GetType().Name.GetHashCode());	//Notice determinism starting always with same seed.
	}

	protected void Init(ComponentType[] all, ComponentType[] any = null) {
		_eventChance = Config.instance.eventChance;
		_batchCount = Config.instance.batchCount;

		//any = any ?? Array.Empty<ComponentType>();
		//_archetype = _emgr.CreateArchetype(
		//	all
		//		.Concat(any)
		//		.Distinct()
		//		.ToArray()
		//	);
		//var query = new EntityArchetypeQuery {
		//	Any = Array.Empty<ComponentType>()
		//	, None = Array.Empty<ComponentType>()
		//	, All = all
		//};
		//_group = GetComponentGroup(query);

		_archetype = _emgr.CreateArchetype(all);
		_group = GetComponentGroup(all);

	}

	protected void CreateEntities() {
		int amt = Config.instance.numEntites;
		var ents = new NativeArray<Entity>(amt, Allocator.Temp);
		_emgr.CreateEntity(_archetype, ents);
		ents.Dispose();
	}

	protected virtual JobHandle OnUpdateImpl(JobHandle inputDeps) { return inputDeps; }

	protected override JobHandle OnUpdate(JobHandle inputDeps) {
		var wait = Config.instance.forceWaitJobs;
		Profiler.BeginSample(_systemGroup + "Upd");
		var ret = OnUpdateImpl(inputDeps);
		if(wait)
			ret.Complete();
		Profiler.EndSample();
		return ret;
	}

	protected override void OnDestroyManager() {
		if(_rngs.IsCreated)
			_rngs.Dispose();
	}
}
