﻿namespace Dispatch {
	using System.Collections.Generic;
	using Unity.Jobs;
	using Unity.Entities;
	using Unity.Collections;
	using Unity.Burst;
	using UnityEngine.Profiling;
	using Unity.Collections.LowLevel.Unsafe;

	//Different namespace, different type!
	struct Dat_UniqueForThisSystem : IComponentData {
		byte _bogus;
	}

	public class Sys_Dispatch_Init : Sys_Base {
		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object) });
			if(Config.instance.sysDispatch)
				CreateEntities();
		}
	}

	[UpdateAfter(typeof(Sys_Dispatch_Init))]
	public class Sys_Dispatch_Raise : Sys_Base {
		[Inject] Sys_Dispatch_Process sysProcess;

		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object) });
			CreateRNGs();
		}

		[BurstCompile]
		struct Job : IJobProcessComponentDataWithEntity<Dat_UniqueForThisSystem, Dat_Object> {
			[NativeDisableParallelForRestriction] public NativeArray<Unity.Mathematics.Random> rngs;
			[NativeSetThreadIndex] int thread;
			public float eventChance;

			public NativeMultiHashMap<int, Dat_EventWithSource>.Concurrent _mapMT;

			public void Execute(Entity entity, int index, [ReadOnly] ref Dat_UniqueForThisSystem dummy, ref Dat_Object obj) {
				var rng = rngs[thread];
				if(rng.NextFloat() < eventChance) {
					//Event occurred
					_mapMT.Add((int)EventType.TheEvent, new Dat_EventWithSource { source = entity, type = EventType.TheEvent, magic = 123 });
				}
				rngs[thread] = rng;
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			return new Job {
				rngs = _rngs
				, eventChance = _eventChance
				, _mapMT = sysProcess._map.ToConcurrent()
			}.Schedule(this, inputDeps);
		}
	}

	[AlwaysUpdateSystem]    //<-- Important
	[UpdateAfter(typeof(Sys_Dispatch_Raise))]
	public class Sys_Dispatch_Process : Sys_Base {
		public NativeMultiHashMap<int, Dat_EventWithSource> _map;

		protected override void OnCreateManager() {
			_map = new NativeMultiHashMap<int, Dat_EventWithSource>(Config.instance.HashMapSize, Allocator.Persistent);

			//Dummy call to link dependencies with other systems (because we don't actually use any components):
			GetComponentGroup(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object) });
		}

		protected override void OnDestroyManager() {
			base.OnDestroyManager();
			_map.Dispose();
		}

		[BurstCompile]
		struct Job_TheEvent : IJob {
			[ReadOnly] public NativeMultiHashMap<int, Dat_EventWithSource> map;
			public ComponentDataFromEntity<Dat_Object> objectArray;
			public int key;

			public void Execute() {
				Dat_EventWithSource ev;
				NativeMultiHashMapIterator<int> it;
				if(map.TryGetFirstValue(key, out ev, out it)) {
					do {
						var obj = objectArray[ev.source];
						obj.magic = ev.magic;
						obj.number++;
						objectArray[ev.source] = obj;
					} while(map.TryGetNextValue(out ev, ref it));
				}
			}
		}

		[BurstCompile]
		struct JobClear : IJob {
			public NativeMultiHashMap<int, Dat_EventWithSource> map;
			public void Execute() {
				map.Clear();
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			//inputDeps.Complete();

			var objectArray = GetComponentDataFromEntity<Dat_Object>();

			var deps = new Job_TheEvent {
				key = (int)EventType.TheEvent
				, objectArray = objectArray
				, map = _map
			}.Schedule(inputDeps);

			deps = new JobClear { map = _map }.Schedule(deps);

			return deps;
		}
	}
}