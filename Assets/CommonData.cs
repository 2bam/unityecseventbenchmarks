﻿using Unity.Entities;
using Unity.Mathematics;
using System;


[Serializable]
public enum EventType : byte {
	None = 0
	, TheEvent
}

[Serializable]
public struct Dat_Object : IComponentData {
	public int number;
	public byte magic;
	public float4 vector;
}

[Serializable]
public struct Dat_EventData : IComponentData {
	public EventType type;
	public byte magic;
}

[Serializable]
public struct Sha_EventData : ISharedComponentData {
	public EventType type;
	public byte magic;
}

[Serializable]
public struct Dat_EventWithSource : IComponentData {
	public Entity source;
	public EventType type;
	public byte magic;
}

