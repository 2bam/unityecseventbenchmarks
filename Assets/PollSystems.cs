﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Jobs;
using Unity.Mathematics;
using Unity.Entities;
using Unity.Collections;
using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;

// RUN ORDER
//
// Sys_Poll...
// Init (once) > Raise > Process

namespace Poll {

	//Different namespace, different type!
	struct Dat_UniqueForThisSystem : IComponentData {
		byte _bogus;
	}

	struct Dat_EventFlag : IComponentData {
		public byte raised;
	}

	public class Sys_Poll_Init : Sys_Base {
		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object), typeof(Dat_EventData), typeof(Dat_EventFlag) });
			if(Config.instance.sysPoll)
				CreateEntities();
		}
	}

	[UpdateAfter(typeof(Sys_Poll_Init))]
	public class Sys_Poll_Raise : Sys_Base {
		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object), typeof(Dat_EventData), typeof(Dat_EventFlag) });
			CreateRNGs();
		}

		[BurstCompile]
		struct Job : IJobProcessComponentDataWithEntity<Dat_UniqueForThisSystem, Dat_Object, Dat_EventData, Dat_EventFlag> {
			[NativeDisableParallelForRestriction] public NativeArray<Unity.Mathematics.Random> rngs;
			[NativeSetThreadIndex] int thread;
			public float eventChance;

			public void Execute(Entity entity, int index, [ReadOnly] ref Dat_UniqueForThisSystem dummy, ref Dat_Object obj, ref Dat_EventData evData, ref Dat_EventFlag evFlag) {
				var rng = rngs[thread];
				if(rng.NextFloat() < eventChance) {
					//Event occurred
					evData.type = EventType.TheEvent;
					evData.magic = 123;

					evFlag.raised = 1;
				}
				rngs[thread] = rng;
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			return new Job {
				rngs = _rngs
				, eventChance = _eventChance
			}.Schedule(this, inputDeps);
		}
	}

	[UpdateAfter(typeof(Sys_Poll_Raise))]
	public class Sys_Poll_Process : Sys_Base {
		[BurstCompile]
		struct Job : IJobProcessComponentDataWithEntity<Dat_UniqueForThisSystem, Dat_Object, Dat_EventData, Dat_EventFlag> {
			public void Execute(Entity entity, int index, [ReadOnly] ref Dat_UniqueForThisSystem dummy, ref Dat_Object obj, ref Dat_EventData evData, ref Dat_EventFlag evFlag) {
				if(evFlag.raised == 1) {
					evFlag.raised = 0;
					if(evData.type == EventType.TheEvent) {
						obj.number++;
						obj.magic = evData.magic;
						evData.type = EventType.None;
					}
				}
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			return new Job {
			}.Schedule(this, inputDeps);
		}
	}
}