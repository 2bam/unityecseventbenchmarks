﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Jobs;
using Unity.Mathematics;
using Unity.Entities;
using Unity.Collections;
using Unity.Burst;
using System;
using Unity.Jobs.LowLevel.Unsafe;
using Unity.Collections.LowLevel.Unsafe;

// RUN ORDER
//
// Sys_PollShared_...
// Init (once) > Raise > RaiseBarrier > Process > ProcessBarrier

namespace PollShared {

	//Different namespace, different type!
	struct Dat_UniqueForThisSystem : IComponentData {
		byte _bogus;
	}

	[Serializable]
	struct Sha_SharedEventFlag : ISharedComponentData {
		public byte raised;
	}

	public class Sys_PollShared_Init : Sys_Base {
		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object), typeof(Dat_EventData), typeof(Sha_SharedEventFlag) });
			if(Config.instance.sysPollShared)
				CreateEntities();
		}
	}

	[UpdateAfter(typeof(Sys_PollShared_Init))]
	public class Sys_PollShared_Raise : Sys_Base {
		[Inject] Sys_PollShared_RaiseBarrier _barrier;

		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object), typeof(Dat_EventData), typeof(Sha_SharedEventFlag) });
			CreateRNGs();
		}

		//Not supported: [BurstCompile]
		struct Job : IJobParallelFor {
			[NativeDisableParallelForRestriction] public NativeArray<Unity.Mathematics.Random> rngs;
			[NativeSetThreadIndex] int thread;
			public float eventChance;
			public EntityCommandBuffer.Concurrent buffer;

			public ComponentDataArray<Dat_Object> objectArray;
			public ComponentDataArray<Dat_EventData> eventDataArray;
			public SharedComponentDataArray<Sha_SharedEventFlag> eventFlagArray;
			public EntityArray entities;

			public void Execute(int index) {
				var rng = rngs[thread];
				if(rng.NextFloat() < eventChance) {
					//Event occurred
					eventDataArray[index] = new Dat_EventData { type = EventType.TheEvent, magic = 123 };
					buffer.SetSharedComponent(index, entities[index], new Sha_SharedEventFlag { raised = 1 });
				}
				rngs[thread] = rng;
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			return new Job {
				eventChance = _eventChance
				, rngs = _rngs
				, buffer = _barrier.CreateCommandBuffer().ToConcurrent()
				, entities = _group.GetEntityArray()
				, objectArray = _group.GetComponentDataArray<Dat_Object>()
				, eventDataArray = _group.GetComponentDataArray<Dat_EventData>()
				, eventFlagArray = _group.GetSharedComponentDataArray<Sha_SharedEventFlag>()
			}.Schedule(_group.CalculateLength(), _batchCount, inputDeps);
		}
	}

	[UpdateAfter(typeof(Sys_PollShared_Raise))]
	public class Sys_PollShared_RaiseBarrier : BarrierSystem { }

	[UpdateAfter(typeof(Sys_PollShared_RaiseBarrier))]
	public class Sys_PollShared_Process : Sys_Base {
		[Inject] Sys_PollShared_ProcessBarrier _barrier;

		protected override void OnCreateManager() {
			Init(new ComponentType[] { typeof(Dat_UniqueForThisSystem), typeof(Dat_Object), typeof(Dat_EventData), typeof(Sha_SharedEventFlag) });
			_group.SetFilter(new Sha_SharedEventFlag { raised = 1 });
		}

		//Not supported: [BurstCompile]
		struct Job : IJobParallelFor {
			public EntityCommandBuffer.Concurrent buffer;

			public ComponentDataArray<Dat_Object> objectArray;
			public ComponentDataArray<Dat_EventData> eventDataArray;
			public SharedComponentDataArray<Sha_SharedEventFlag> eventFlagArray;
			public EntityArray entities;

			public void Execute(int index) {			
				//if not needed, using ComponentGroup.SetFilter
				//if(eventFlagArray[index].raised == 1) {
					var obj = objectArray[index];
					var evData = eventDataArray[index];

					if(evData.type == EventType.TheEvent) {
						obj.number++;
						obj.magic = evData.magic;
						evData.type = EventType.None;
					}

					objectArray[index] = obj;
					eventDataArray[index] = evData;
					buffer.SetSharedComponent(index, entities[index], new Sha_SharedEventFlag { raised = 0 });

				//}
			}
		}

		protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
			return new Job {
				buffer = _barrier.CreateCommandBuffer().ToConcurrent()
				, entities = _group.GetEntityArray()
				, objectArray = _group.GetComponentDataArray<Dat_Object>()
				, eventDataArray = _group.GetComponentDataArray<Dat_EventData>()
				, eventFlagArray = _group.GetSharedComponentDataArray<Sha_SharedEventFlag>()
			}.Schedule(_group.CalculateLength(), _batchCount, inputDeps);
		}
	}

	[UpdateAfter(typeof(Sys_PollShared_Process))]
	public class Sys_PollShared_ProcessBarrier : BarrierSystem { }

}

