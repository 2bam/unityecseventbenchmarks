﻿# UnityECSEventBenchmarks #

Reference
---------
* Dat_ / CD:			ComponentData
* Sha_ / SCD:			SharedComponentData
* Sys_:				System
* System-group/SG:	A group of related systems to handle test event handling in different ways. They share a namespace.

Tests
-----
This tries to test a real-case scenario with randomness and some struct data manipulation.
It's far from perfect, I'm glad to accept corrections and suggestions.

CommonData.cs contains some CD and SCD structs used to test in all system groups.
CommonSystems.cs has a utility Sys_Base that provides some default injections, a random number generator, archetype,
	group, etc. (Remember to call base.OnDestroyManager() on overrides!)

"Dat_UniqueForThisSystem" CD, even having the same name are actually different structs for each system-group.
	Because of their different namespaces, this avoids incorrect processing across/between system-groups.

When running multiple system-groups, you should enable forced wait on completion (see Config below), otherwise jobs would
"bleed into" other system-groups' time, holding worker threads and giving harder to recognize timings in the profiler.
Config booleans avoid creation of entities and destroy unwanted systems.

Versions used:

	Unity 2018.2.18f1
	com.unity.jobs@0.0.7-preview.5
	com.unity.entities@0.0.12-preview.21
	com.unity.collections@0.0.9-preview.10

Systems definition syntax is "Sys_{System-group}_{Step}", and contained in "{System-Group}Systems.cs". All explained below.

System-groups
-------------
**Poll**:		Set a CD raised value to flag as event raised. Unset when processing.
				While processing modifies CD in-place, Burst compiled.

**PollShared**:	Set a SCD raised value to flag as event raised. Unset when processing.
				This will move entities between chunks, but can be filtered easier (consistent branching).
				Uses barriers to set SCD, so NO Burst compiling.
				1 event per entity.

**Tag**:		Add a CD "tag" when event is raised. Remove CD when processing.
				Uses barriers to add/remove, so NO Burst compiling.
				1 event per entity.

**TagShared**:	Add a SCD "tag" when event is raised. Remove SCD when processing.
				Uses barriers to add/remove, so NO Burst compiling.
				1 event per entity.

**EvQueue**:	Uses a Queue to store and then apply events to objects using ComponentDataFromEntity.
				No extra components or entities.
				Multiple events per entity.

**EvQueueHM**:	Uses a Queue and HashMultiMap to store and then apply events to objects using
				ComponentDataFromEntity.
				As the map is by entity, it can get parallelized when processing.
				No extra components or entities.
				Multiple events per entity.

**EntEv**:		Creates one entity per event to keep the ECS flow.
				Queues events and turns them into "entities with just the one event CD" via
				CreateEntities and ComponentDataFromEntity.
				These event-entities last 1 frame.
				Multiple events per entity.

**Dispatch**:	Uses a HashMultiMap to map event type to a queue, then applying using ComponentDataFromEntity.
				Jobs by event-type could be sequenced or in parallel.
				No extra components or entities.
				Multiple events per entity.

Steps
-----
**_Init**:			Create entities for that system group (if Config.instance.sys* boolean set)

**_Raise**:			Raise events. Uses Config.instance.eventChance and a random number generator to 

**_CreateEvents**:	(Only for EntEv/Batch SG) All queued events are asigned to newly created entities. These entities last 1 frame.

**_Process**:		Do processing of the events, modify the object.

**_RaiseBarrier** and **_ProcessBarrier** are optional and only used when buffering is used.

The dependency sequence is:

**Init (once) > Raise [> RaiseBarrier > CreateEvents > BatchSystem] > Process [> ProcessBarrier]**

Config
------
**numEntities**:	Amount of entities to spawn PER SYSTEM-GROUP.

**eventChance**:	to define probability of events happening (0..1)

**batchCount**:		Parameter to pass as batch count for IJobParallelFor scheduling.

**sys\* booleans**:	Enable or disable system-groups' entity creation.
					Also uses some Reflection magic to destroy related systems at Start(), for better profiling.

**forceWaitJobs**:	Wait for jobs on every system.
					Makes profiler graphs more readable, to know how much each system takes.
					For more realistic timing, enable ONLY ONE system and disable waiting.

**hashMapSizeAccordingToChanceAndEntities**: Auto adjust hash size by chance and num entities.

