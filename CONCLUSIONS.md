﻿Conclusions
-----------
PollShared, Tag and TagShared spend all their time on barrier command replay. Buffers are not Burst-friendly neither.

Raised events sources have coherency with entity order, to get a better real world scenario maybe would be better
to use random entity targets instead of source entity for modification through ComponentDataFromEntity.
Checking for (ev.type == EventType.TheEvent) is also very coherent (will always be true) so it's not a great test
for multiple events handled in the same place.

Poll is the best for single events per entity and for big volumes.
EvQueue is the best for multiple events per entity, it cuts the middle man (adding component to entities).
Batch is the best for multiple events per entity and also keeping the convenience of ComponentSystem encapsulation and automatic filtering.

For single events, Batch runs for the same time as polling for a <2% event volume (2% chance entities raise an event per frame) for 100k to 10 million entities.

EntEv is practically Batch without direct chunk assignments. ComponentDataFromEntity takes 3.5x than writing to chunks directly.
For low event volumes (<10%) EntEv starts running faster than Batch (but direct chunk job still is faster). Maybe because of less GC allocs?

Dispatch (uses HashMultiMap as queue) takes 10x than EvQueue for 100% event volume at 100k entities, takes around the same time at 10% event volume.
Could be good for multiple events with different jobs and modularity instead of if/switch on them.

Note that Poll should be always almost unaffected by chance.

FPS test
--------
10 million entities
100% event volume (chance entities raise an event per frame)
Includes timing from engine.
Not forcing wait on jobs

Poll 100ms, EvQueue 280ms, Batch 520ms, EntEv 750ms, Dispatch 1200ms

Specs
-----
i7-4790k @ 4Ghz, 16G ram, 4 core, 8 thread, cache 4x32KB L1, 4x256KB L2, 8MB L3
